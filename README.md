# Thuglife

There's a thug in town !

  - Créez un fichier html avec une div id "wrapper"

  - Avec la balise script, lier un fichier javascript à votre html.

  - Avec la méthode document.getElementById(), récupérez cette div "wrapper" dans votre javascript et gardez-la dans une var.

  - Créez var random avec une valeur Math.random() de 0 à 120.

  - Faites une boucle for() avec une valeur var i allant de 0 à 120.

  - Si la valeur de i == random. Utilisez la méthode .innerHTML, pour ajouter une div html avec une img ./img/thugcat.jpg.

  - Sinon, utilisez la méthode .innerHTML pour ajouter une div avec une img dont la source sera ./img/cat.jpg.